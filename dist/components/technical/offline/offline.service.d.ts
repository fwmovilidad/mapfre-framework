import { Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Observable } from 'rxjs/Observable';
export declare class MapfreNetworkService {
    network: Network;
    platform: Platform;
    private onDevice;
    constructor(network: Network, platform: Platform);
    isOnline(): boolean;
    getNetType(): string;
    onChange(): Observable<any>;
    onConnect(): Observable<any>;
    onDisconnect(): Observable<any>;
}
