import { ElementRef, EventEmitter, OnInit, Renderer2 } from '@angular/core';
import { ReadFile } from './read-file';
import { ReadMode } from './read-mode.enum';
export declare class FilePickerDirective implements OnInit {
    private el;
    private renderer;
    accept: string;
    multiple: any;
    readMode: ReadMode;
    filePick: EventEmitter<ReadFile>;
    readStart: EventEmitter<number>;
    readEnd: EventEmitter<number>;
    private _multiple;
    private input;
    constructor(el: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    reset(): void;
    browse(): void;
    private readFile;
}
