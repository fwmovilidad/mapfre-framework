import { ReadFile } from './read-file';
import { ReadMode } from './read-mode.enum';
export declare class ReadFileImpl implements ReadFile {
    private _underlyingFile;
    private _readMode;
    private _content;
    readonly lastModifiedDate: Date;
    readonly name: string;
    readonly size: number;
    readonly type: string;
    readonly readMode: ReadMode;
    readonly content: any;
    readonly underlyingFile: File;
    constructor(_underlyingFile: File, _readMode: ReadMode, _content: any);
}
