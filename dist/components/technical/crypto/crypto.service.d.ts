import '../../../declarations';
export declare class CryptoService {
    private crypto;
    private msCrypto;
    private webkitSubtle;
    private key;
    private enc;
    private dec;
    private algorithm;
    constructor();
    private init;
    getKey(): PromiseLike<CryptoKey>;
    encrypt(key: CryptoKey, data: any): PromiseLike<ArrayBuffer>;
    decrypt(data: any): any;
}
