import { Platform, IonicErrorHandler } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import '../../../declarations';
export declare class MapfreErrorHandler extends IonicErrorHandler {
    private platform;
    private splash;
    constructor(platform: Platform, splash: SplashScreen);
    handleError(error: any): void;
    reloadApp(): void;
    crashlyticsError(error: any): void;
}
