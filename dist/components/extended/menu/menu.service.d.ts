import { Subject } from 'rxjs/Subject';
export interface Option {
    icon?: string;
    iconImg?: string;
    text: string;
    subText?: string;
    subIcon?: string;
    page?: any;
    url?: string;
    selected?: boolean;
    id?: string;
    badgeNumber?: number;
}
export interface MenuOption {
    option: Option;
    subOptions?: MenuOption[];
    type?: number;
}
export interface MenuFooterOption {
    icon?: string;
    iconImg?: string;
    text?: string;
    page?: any;
    url?: string;
    type?: string;
    id: string;
    subOptions?: MenuFooterOption[];
}
export declare class MenuService {
    private avatar;
    private avatarText;
    private menuOptions;
    menuOptionsChange: Subject<MenuOption[]>;
    private menuFooterOptions;
    menuFooterOptionsChange: Subject<MenuFooterOption[]>;
    constructor();
    getAvatar(): string;
    setAvatar(avatar: string): void;
    getAvatarText(): string;
    setAvatarText(avatarText: string): void;
    getMenuOptions(): MenuOption[];
    setMenuOptions(menuOptions: MenuOption[]): void;
    setMenuFooterOptions(menuFooterOptions: MenuFooterOption[]): void;
    updateSelectedOption(option_id: string, deselectAll?: boolean): void;
    updateNotificationBadge(unreadNumber: number): void;
}
