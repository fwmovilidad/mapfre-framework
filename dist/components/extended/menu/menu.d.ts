import { EventEmitter } from '@angular/core';
import { MenuOption, MenuService, Option, MenuFooterOption } from './menu.service';
import { MenuController } from 'ionic-angular';
export declare class MapfreMenu {
    menu: MenuService;
    menuCtrl: MenuController;
    content: any;
    persistent: boolean;
    swipeEnabled: boolean;
    menuLogo: string;
    avatar: string;
    title: string;
    subTitle: string;
    menuOptions: MenuOption[];
    menuFooterOptions: MenuFooterOption[];
    optionClicked: EventEmitter<Event>;
    constructor(menu: MenuService, menuCtrl: MenuController);
    ngOnInit(): void;
    doOptionClicked(event: Event, optionName: Option, optionFather?: Option, optionGrandFather?: Option): void;
}
