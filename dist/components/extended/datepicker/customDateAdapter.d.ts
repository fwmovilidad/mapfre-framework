import { NativeDateAdapter } from '@angular/material';
export declare class CustomDateAdapter extends NativeDateAdapter {
    getFirstDayOfWeek(): number;
}
