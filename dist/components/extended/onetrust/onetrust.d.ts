import { EventEmitter } from '@angular/core';
import { OneTrustService } from './onetrust.service';
interface Purpose {
    Id: string;
    text: string;
    status: boolean;
    required: boolean;
}
export declare class MapfreOneTrust {
    private otService;
    private rgpd;
    title: string;
    text: string;
    purposes: Purpose[];
    textButton: string;
    identifier: string;
    requestInformation: string;
    test: boolean;
    apikey: string;
    url: any;
    onetrustResult: EventEmitter<Object>;
    private showText;
    constructor(otService: OneTrustService);
    ngAfterViewInit(): void;
    stringControl(texto: any): any;
    enviarForm(): void;
    isButtonDisabled(): boolean;
    handleChange(event: any, opcion: any): void;
    private controlObj;
    showTextComplete(elemento: any): any;
}
export {};
