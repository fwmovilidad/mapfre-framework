import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
export declare class OneTrustService {
    private _http;
    constructor(_http: HttpClient);
    registrarConsentimiento(url: any, identifier: any, requestInformation: any, purposes: any, test: any): Observable<Response>;
    consultaOpcionesAceptadasPorUsuario(identifier: any, url: any, apikey: any): Observable<Response>;
    revocarConsentimiento(identifier: any, purpose: any, url: any, apikey: any): Observable<Response>;
}
