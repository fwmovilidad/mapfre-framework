import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { HttpClient } from '@angular/common/http';
export declare class UploaderService {
    private _http;
    constructor(_http: HttpClient);
    getPresignedUrl(params: any): Observable<any>;
    postFileToS3(url: any, document: any): Observable<any>;
    deleteFile(params: any): Observable<any>;
    deleteFileSuffixPrefix(params: any): Observable<any>;
    private handleError;
}
