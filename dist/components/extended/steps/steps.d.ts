import { EventEmitter, OnInit, ElementRef } from '@angular/core';
export interface MapfreStep {
    title?: string;
    subtitle?: string;
    status?: string;
    active?: boolean;
}
export declare class MapfreSteps implements OnInit {
    hideScrollButtons: Boolean;
    selectedStep: MapfreStep;
    selectedStepIndex: number;
    private stepsContainer;
    orientation: string;
    steps: MapfreStep[];
    height: string;
    mClick: EventEmitter<Event>;
    constructor(overflowContainer: ElementRef, stepsContainer: ElementRef);
    private initActiveState;
    protected doBulletClick(event: Event, index: number): void;
    nextStep(): void;
    previousStep(): void;
    ngOnInit(): void;
}
