import { OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';
export declare class MapfrePopover implements OnInit {
    viewCtr: ViewController;
    constructor(viewCtr: ViewController);
    options: string[];
    doOptionClicked(myEvent: Event, option: string): void;
    ngOnInit(): void;
    controlDismiss(listSize: any, index: any): void;
    controlKeyDown(myEvent: Event, option: string): void;
}
