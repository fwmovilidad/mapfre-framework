import { EventEmitter, OnInit, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { PopoverController, Platform, ViewController } from 'ionic-angular';
import { MenuService, MenuOption, Option } from '../menu/menu.service';
export declare class MapfreHeader implements OnInit {
    popoverCtrl: PopoverController;
    menuService: MenuService;
    platform: Platform;
    private renderer;
    private ren2;
    private viewCtr;
    constructor(popoverCtrl: PopoverController, menuService: MenuService, platform: Platform, renderer: Renderer, ren2: Renderer2, viewCtr: ViewController);
    options: MenuOption[];
    avatarText: string;
    avatar: string;
    menuOptionHover: MenuOption;
    title: string;
    titleImg: string;
    backArrow: boolean;
    menu: boolean;
    rightIcons: string[];
    actionMenu: string[];
    iconClicked: EventEmitter<Event>;
    titleClicked: EventEmitter<Event>;
    avatarClicked: EventEmitter<Event>;
    actionMenuClicked: EventEmitter<Event>;
    menuClicked: EventEmitter<Event>;
    listaSubmenu: ElementRef;
    mainOptions: ElementRef;
    mainMenu: ElementRef;
    rightIconsEl: ElementRef;
    popover: ElementRef;
    private elementsSubmenu;
    private optionSelected;
    ngOnInit(): void;
    doIconClicked(myEvent: Event, iconName: string): void;
    doTitleClicked(myEvent: Event): void;
    doAvatarClicked(myEvent: Event): void;
    doActionMenuClicked(myEvent: any): void;
    doMenuClicked(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
    doMenuClickedExtra(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
    doMenuClickedKey(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
    doMenuClickedKeyExtra(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
    mouseEnter(menuOption: MenuOption): void;
    mouseLeave(): void;
    close(event: any): void;
    keyEnter(menuOption: MenuOption, pos: number): void;
    nextFocusExtra(menuOption: MenuOption): void;
    nextFocus(menuOption: MenuOption, indexSuboption?: number, indexSubSub?: number): void;
    controlKeyActionMenu(event: any): void;
    controlKeyPress(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
    controlKeyPressExtra(myEvent: Event, option: Option, optionFather?: Option, optionGrandFather?: Option): void;
}
