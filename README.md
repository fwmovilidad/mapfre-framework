     --- GUÍA DE INSTALACIÓN ---

El framework de movilidad de Mapfre es un empaquetado de componentes basados en Ionic y alojados en un repositorio npm. 
Toda la información sobre el framework se encuentra en http://fwmovilidad.mapfre.com/

Para hacer uso del framework, es necesario instalar:

Nodejs (v7.x o superior)


 --- Instalación en un proyecto en marcha ---
El framework de componentes de Mapfre está basado en las siguientes versiones:

  Angular 4.x
  Ionic 3.x

Para usar los componentes de Mapfre en una aplicación en marcha seguir los siguientes pasos:

1 Instalar el paquete mapfre-components.

  npm install --save mapfre-components

Aparecerá el paquete en la carpeta node_modules/.

2 Importar el ComponentsModule en el app.module

  import { ComponentsModule } from 'mapfre-framework';

y añadirlo a la sección de imports del @NgModule de Angular.

3 Importar Menu en el app.components,

  import { MenuOption} from 'mapfre-framework';

inicializarlo con las opciones del menú

this.menuOptions = [
      {
        option: {
          icon: '<icon_1>',
          text: '<text_1>',
        }
      },
      ...
      ,
      {
        option: {
          icon: '<icon_n>',
          text: '<text_n>',
        } 
      }];

y añadirlo al template/templateUrl de la página

<mapfre-menu [content]="mycontent" [title]="'Lorem ipsum'" [subTitle]="'Lorem ipsun'" [menuOptions]="menuOptions" (optionClicked)="doOptionClicked($event)"></mapfre-menu>
<ion-nav #mycontent [root]="rootPage"></ion-nav>


 --- Instalación con el arquetipo de Mapfre ---

Mapfre dispone de un proyecto arquetipo con el que poder empezar a desarrollar. Esta es la manera que se recomienda, pues evita posibles problemas con la integración de diferentes versiones de librerías externas que pueden hacer que la aplicación no funcione correctamente.

1 Descargar la aplicación desde la página de descargas del portal del desarrollador

2 Instalar dependencias

  npm install
  
Una vez finalizada la instalación, estará listo para empezar a desarrollar.
