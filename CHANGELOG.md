# FRAMEWORK MOVILIDAD MAPFRE
## mapfre-framework 1.2.25 (15 de Noviembre 2018)
### Hotfix
- En componente OneTrust: Dependencias de onetrust modificadas.

## mapfre-framework 1.2.24 (14 de Noviembre 2018)
### Fixed
- En componente OneTrust: Añadidos inputs apikey y url para poder modificarlas desde fuera de la librería.

## mapfre-framework 1.2.23 (13 de Noviembre 2018)
### Add
- En componente Uploader: Posibilidad de añadir scroll a la lista de documentos subidos.
- En componente Uploader: @Input items para el número máximo de archivos subidos que se mostrarán en la lista.

## mapfre-framework 1.2.22 (8 de Noviembre 2018)
### Fixed
- Alto de Header ajustable a contenido.
- Modificados enlaces a la API de onetrust.

## mapfre-framework 1.2.21 (7 de Noviembre 2018)
### Add
- Lista con nombre de archivos subidos en componente mapfre-uploaderfiles

## mapfre-framework 1.2.20 (31 de Octubre 2018)
### Fixed
- Funcionamiento de componente uploader

## mapfre-framework 1.2.19 (13 de Septiembre 2018)
### Fixed
- Arreglado el popover del ion-select.
- Navegación por teclado en el Popover del Header.

## mapfre-framework 1.2.18 (6 de Septiembre 2018)
### Add 
- Propiedad mPropertyId en (menuClicked) de mapfre-header con la id de la opción seleccionada en el menú. 
### Fixed
- Arreglada maquetación del select para que se adapte al ancho de pantalla en opciones más largas.
- Modificado tamaño del título de mapfre-onetrust para poder añadir títulos más largos.
- Arreglados fallos en la navegación por teclado del mapfre-header.

## mapfre-framework 1.2.17 (7 de Agosto 2018)
### Fixed
- Implementada la posibilidad de navegar por el header con el teclado.

## mapfre-framework 1.2.16 (6 de Agosto 2018)
### Fixed
- Arreglado componente onetrust para que se construya de forma correcta.
### Added
- Métodos de borrado de archivos en el servicio del mapfre-uploaderfiles

## mapfre-framework 1.2.15 (3 de Agosto 2018)
### Fixed
- Añadido @Output a mapfre-onetrust con resultado de la respuesta o el error de enviar el formulario de registro de propósitos

## mapfre-framework 1.2.14 (1 de Agosto 2018)
### Fixed
- Modificado nombre del componente a onetrust por buenas prácticas.
- Añadido scroll en onetrust-component.
- El parámetro texto de onetrust admite texto enriquecido.

## mapfre-framework 1.2.13 (17 de Julio 2018)
### Added
- Componente onetrust para registrar el contrato RGPD.
- Componente uploaderfiles para subida de archivos.
- Componente con directiva explorerfiles para que cualquier elemento html pueda abrir el explorador de archivos de un dispositivo.

## mapfre-framework 1.2.12 (27 de Junio 2018)
### Fixed
- Se ha modificado el submenú del header para escritorio: Ahora al pasar el ratón por encima de las opciones se sombrea todo el 
  espacio de la opción.
- Se ha adaptado el ancho del select: Si existe en una select alguna opción más larga, el contenedor modifica su ancho hasta un máximo
  que será el ancho de la pantalla del dispositivo en que se utiliza.

## mapfre-framework 1.2.11 (28 de Mayo 2018)
### Changed
- Se ha modificado el menú, para poder visualizar si se desea las notificaciones pendientes en la aplicación, además de poder 
  pintar opciones en el footer del menú y algunos cambios visuales para adaptar la visualización de Android e iOS. 
### Fixed
- Solución de error para que el menú del header pueda cerrarse desde todos los dispositivos cuando haces click fuera de él.

## mapfre-framework 1.2.10 (08 de Mayo 2018)
### Fixed
- Solución de error para que los select que se desplieguen como popover se muestren correctamente.

## mmapfre-framework 1.2.9 (27 de Abril 2018)
### Fixed
- Cambio de estilos en el header para que haya hover en los botones clickables del submenú.
- Cambios de estilos en el popover del header para que se muestre siempre como el de iOS.

